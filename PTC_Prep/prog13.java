//Strong Number is yes or not
//
//i/p:145
//o/p: 145 is a Strong No.
//
class ptc{

	public static void main(String[]args){


		int num=145;
		int sum=0;
		int temp=num;

		while(num!=0){

			int rem=num%10;
			int fact=1;

		while(rem!=0){

			fact=fact*rem;
			rem--;
		}
	        sum=sum+fact;
		num=num/10;
		}
		if(temp==sum){

			System.out.println(temp+"is a Strong Number");
		}else{
			System.out.println(temp+"is not a Strong Number");
		}
	}
}
