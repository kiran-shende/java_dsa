//Print Reverse Number
//
//i/p:132
//o/p:231
//
class ptc{

	public static void main(String[]args){

		int num=132;
		int rev=0;
		while(num!=0){


			int rem=num%10;
		
		       
			rev=rev*10+rem;
			num=num/10;
		}
		System.out.println(rev);
	}
}

